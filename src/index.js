const WebSocket = require('ws');
const { v4: uuidv4 } = require('uuid');

global.socketMap = {};
global.hostClientMap = {};
global.clientHostMap = {};

const newId = () => {
  var tmp = uuidv4();
  while(true) {
    if(typeof global.socketMap[tmp] === 'undefined') {
      return tmp;
    }
    tmp = uuidv4();
  }
}

const wss = new WebSocket.Server({ port: 2122 });
wss.on('connection', (ws) => {
  var id = newId();
  global.socketMap[id] = ws;

  ws.on('message', (msgStr) => {
    var msg = JSON.parse(msgStr);
    if(msg.type === 'host' && typeof global.socketMap[msg.data] !== 'undefined') {
      global.hostClientMap[msg.data] = msg.id;
      global.clientHostMap[msg.id] = msg.data;
      global.socketMap[msg.data].send(JSON.stringify({ type: 'connect' }));
      console.log(`New game! Host: ${msg.data} Client: ${msg.id}`);
    }
    else {
      if(typeof global.hostClientMap[msg.id] === 'string') {
        global.socketMap[global.hostClientMap[msg.id]].send(msgStr);
      }
      else if(typeof global.clientHostMap[msg.id] === 'string') {
        global.socketMap[global.clientHostMap[msg.id]].send(msgStr);
      }
      console.log(`New msg from connection: ${msg.id}`);
      console.log(JSON.stringify(msg));
    }
  });
  
  const closeHandler = new Function(`
    if(typeof global.hostClientMap['${id}'] === 'string') {
      if(typeof global.socketMap[global.hostClientMap['${id}']] !== 'undefined') {
        global.socketMap[global.hostClientMap['${id}']].send(JSON.stringify({ type: 'close' }));
      }
      global.clientHostMap[global.hostClientMap['${id}']] = undefined;
      delete global.clientHostMap[global.hostClientMap['${id}']];
      global.hostClientMap['${id}'] = undefined;
      delete global.hostClientMap['${id}'];
    }
    if(typeof global.clientHostMap['${id}'] === 'string') {
      if(typeof global.socketMap[global.clientHostMap['${id}']] !== 'undefined') {
        global.socketMap[global.clientHostMap['${id}']].send(JSON.stringify({ type: 'close' }));
      }
      global.hostClientMap[global.clientHostMap['${id}']] = undefined;
      delete global.hostClientMap[global.clientHostMap['${id}']];
      global.clientHostMap['${id}'] = undefined;
      delete global.clientHostMap['${id}'];
    }
    if(typeof global.socketMap['${id}'] !== 'undefined') {
      global.socketMap['${id}'] = undefined;
      delete global.socketMap['${id}'];
    }
    console.log('Closing client: ${id}');
  `);
  ws.on('close', closeHandler);
  console.log(`New connection: ${id}`);
  ws.send(JSON.stringify({ type: 'self', data: id }));
});

console.log('Server Listening on port: 2122');